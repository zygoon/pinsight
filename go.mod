module gitlab.com/zygoon/pinsight

go 1.22

toolchain go1.22.2

require (
	gitlab.com/zygoon/go-cmdr v1.9.0
	gitlab.com/zygoon/go-raspi v1.4.0
)
