// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Zygmunt Krynicki

package main

import (
	"context"
	"flag"
	"fmt"
	"io"
	"os"
	"text/tabwriter"

	"gitlab.com/zygoon/go-cmdr"
	"gitlab.com/zygoon/go-cmdr/flagcmd"
	"gitlab.com/zygoon/go-raspi/pimodel"
)

type mainCmd struct {
	revcode pimodel.RevisionCode
}

func (cmd *mainCmd) Run(ctx context.Context, args []string) error {
	return flagcmd.MakeCmd(cmd).Run(ctx, args)
}

func (cmd *mainCmd) FlagSet(ctx context.Context) *flag.FlagSet {
	fl := flag.NewFlagSet(cmdr.Name(ctx), flag.ContinueOnError)

	fl.Var(TextValue[pimodel.RevisionCode, *pimodel.RevisionCode]{V: &cmd.revcode}, "r", "Revision code (default: probe)")

	return fl
}

func (cmd *mainCmd) FlagUsage(fl *flag.FlagSet) {
	const msg = `Usage: %s [-r REVCODE]

The program looks for the Raspberry Pi **revision code**. This number contains
information such as the type of the board, the amount of memory or hardware
revision number. You can learn more about revision codes from the official
documentation:

https://www.raspberrypi.com/documentation/computers/raspberry-pi.html#raspberry-pi-revision-codes

If a revision code is given with -r, probing is disabled.
`

	_, _ = fmt.Fprintf(fl.Output(), msg, fl.Name())
}

func (cmd *mainCmd) RunFlag(ctx context.Context, fl *flag.FlagSet) error {
	_, stdout, _ := cmdr.Stdio(ctx)
	revcode := cmd.revcode
	serial := pimodel.SerialNumber(0)

	if revcode == 0 {
		var err error

		revcode, serial, err = pimodel.ProbeCPUInfo(pimodel.ProcCPUInfoFile)
		if err != nil {
			return err
		}
	}

	w := tabwriter.NewWriter(stdout, 0, 0, 2, ' ', 0)

	defer func() {
		_ = w.Flush()
	}()

	showErrorIndent2 := func(w io.Writer, err error) {
		_, _ = fmt.Fprintf(w, "  Error:\t%v\n", err)
	}

	showErrorIndent4 := func(w io.Writer, err error) {
		_, _ = fmt.Fprintf(w, "    Error:\t%v\n", err)
	}

	_, _ = fmt.Fprintf(w, "Raspberry Pi revision code:\t%s\n", revcode)

	if serial != 0 {
		_, _ = fmt.Fprintf(w, "SoC serial number:\t%s\n", serial)
	}

	if soc, err := revcode.Processor(); err == nil {
		_, _ = fmt.Fprintf(w, "Processor:\t%s\n", soc)
		_, _ = fmt.Fprintf(w, "  Silicon family code:\t%d\n", soc.BcmSiliconFamilyCode())
		_, _ = fmt.Fprintf(w, "  Package code:\t%d\n", soc.BcmPackageCode())
	} else {
		_, _ = fmt.Fprintf(w, "Processor:\t?\n")
		showErrorIndent2(w, err)
	}

	if memorySize, err := revcode.MemorySize(); err == nil {
		_, _ = fmt.Fprintf(w, "Memory size:\t%dMiB\n", memorySize)
	} else {
		_, _ = fmt.Fprintf(w, "Memory size:\t?\n")
		showErrorIndent2(w, err)
	}

	if boardType, err := revcode.BoardType(); err == nil {
		_, _ = fmt.Fprintf(w, "Board type:\t%s\n", boardType)

		if boardRev, err := revcode.BoardRevision(); err == nil {
			_, _ = fmt.Fprintf(w, "  Board revision:\t%s\n", boardRev)
		} else {
			_, _ = fmt.Fprintf(w, "  Board revision:\t?\n")
			showErrorIndent4(w, err)
		}
	} else {
		_, _ = fmt.Fprintf(w, "Board type:\t?\n")
		showErrorIndent2(w, err)
	}

	if mfgr, err := revcode.Manufacturer(); err == nil {
		_, _ = fmt.Fprintf(w, "Manufacturer:\t%s\n", mfgr)
	} else {
		_, _ = fmt.Fprintf(w, "Manufacturer:\t?\n")
		showErrorIndent2(w, err)
	}

	_, _ = fmt.Fprintf(w, "Can read OTP bits:\t%v\n", revcode.CanReadOTP())
	_, _ = fmt.Fprintf(w, "Can write OTP bits:\t%v\n", revcode.CanWriteOTP())
	_, _ = fmt.Fprintf(w, "Can use over-voltage:\t%v\n", revcode.CanUseOverVoltage())
	_, _ = fmt.Fprintf(w, "Warranty void by over-voltage:\t%v\n", revcode.WarrantyVoid())

	return nil
}

func main() {
	cmdr.RunMain(new(mainCmd), os.Args)
}
