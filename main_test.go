// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Zygmunt Krynicki

package main_test

import (
	"testing"

	"gitlab.com/zygoon/go-cmdr/cmdtest"
	main "gitlab.com/zygoon/pinsight"
)

func TestHelp(t *testing.T) {
	inv := cmdtest.Invoke(&main.Cmd{}, "--help")

	if err := inv.ExpectExitCode(64); err != nil {
		t.Error(err)
	}

	const expected = `<IGNORE>
Usage: pinsight [-r REVCODE]

The program looks for the Raspberry Pi **revision code**. This number contains
information such as the type of the board, the amount of memory or hardware
revision number. You can learn more about revision codes from the official
documentation:

https://www.raspberrypi.com/documentation/computers/raspberry-pi.html#raspberry-pi-revision-codes

If a revision code is given with -r, probing is disabled.
`

	if err := inv.ExpectStderr(expected); err != nil {
		t.Error(err)
	}

	if err := inv.ExpectStdout(""); err != nil {
		t.Error(err)
	}
}

func TestOutput(t *testing.T) {
	t.Run("pi02w-1", func(t *testing.T) {
		inv := cmdtest.Invoke(&main.Cmd{}, "-r", "902120")

		if err := inv.ExpectExitCode(0); err != nil {
			t.Error(err)
		}

		if err := inv.ExpectStderr(""); err != nil {
			t.Error(err)
		}

		const expected = `<IGNORE>
Raspberry Pi revision code:     902120
Processor:                      RP3A0
  Silicon family code:          2710
  Package code:                 2837
Memory size:                    512MiB
Board type:                     Raspberry Pi Zero 2 W
  Board revision:               1.0
Manufacturer:                   Sony UK
Can read OTP bits:              true
Can write OTP bits:             true
Can use over-voltage:           true
Warranty void by over-voltage:  false
`

		if err := inv.ExpectStdout(expected); err != nil {
			t.Error(err)
		}
	})

	t.Run("pi02w-1", func(t *testing.T) {
		inv := cmdtest.Invoke(&main.Cmd{}, "-r", "a02082")

		if err := inv.ExpectExitCode(0); err != nil {
			t.Error(err)
		}

		if err := inv.ExpectStderr(""); err != nil {
			t.Error(err)
		}

		const expected = `<IGNORE>
Raspberry Pi revision code:     a02082
Processor:                      BCM2837
  Silicon family code:          2710
  Package code:                 2837
Memory size:                    1024MiB
Board type:                     Raspberry Pi 3 Model B
  Board revision:               1.2
Manufacturer:                   Sony UK
Can read OTP bits:              true
Can write OTP bits:             true
Can use over-voltage:           true
Warranty void by over-voltage:  false
`

		if err := inv.ExpectStdout(expected); err != nil {
			t.Error(err)
		}
	})
}
