// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Zygmunt Krynicki

package main

import (
	"encoding"
	"fmt"
)

// TextValue implements flag.Value with TextMarshaler/TextUnmarshaler.
type TextValue[T any, ptrT interface {
	*T
	encoding.TextMarshaler
	encoding.TextUnmarshaler
}] struct {
	V ptrT
}

func (v TextValue[T, ptrT]) String() string {
	data, err := v.V.MarshalText()
	if err != nil {
		return fmt.Sprintf("err: %v", err)
	}

	return string(data)
}

func (v TextValue[T, ptrT]) Set(s string) error {
	return v.V.UnmarshalText([]byte(s))
}
