# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: Huawei Inc.
# SPDX-FileCopyrightText: Zygmunt Krynicki

stages:
  - compliance
  - test
  - build
  - deploy

include:
  - template: 'Workflows/MergeRequest-Pipelines.gitlab-ci.yml'
  - project: 'zygoon/go-gitlab-ci'
    ref: v0.5.0
    file: '/go.yml'

dco:
  interruptible: true
  stage: compliance
  image: christophebedard/dco-check:latest
  tags: [linux, docker, x86_64]
  rules:
    - if: $CI_MERGE_REQUEST_ID
    - if: $CI_EXTERNAL_PULL_REQUEST_IID
    - if: $CI_COMMIT_BRANCH == '$CI_DEFAULT_BRANCH'
  script:
    # Work around a bug in dco-check affecting pipelines for merge requests.
    # https://github.com/christophebedard/dco-check/issues/104
    - |
        if [ "${CI_MERGE_REQUEST_EVENT_TYPE:-}" = detached ]; then
            git fetch -a  # so that we can resolve branch names below
            export CI_COMMIT_BRANCH="$CI_COMMIT_REF_NAME";
            export CI_COMMIT_BEFORE_SHA="$CI_MERGE_REQUEST_DIFF_BASE_SHA";
            export CI_MERGE_REQUEST_SOURCE_BRANCH_SHA="$(git rev-parse "origin/$CI_MERGE_REQUEST_SOURCE_BRANCH_NAME")";
            export CI_MERGE_REQUEST_TARGET_BRANCH_SHA="$(git rev-parse "origin/$CI_MERGE_REQUEST_TARGET_BRANCH_NAME")";
        fi
    - dco-check --default-branch-from-remote --verbose

reuse:
  interruptible: true
  tags: [linux, docker, x86_64]
  stage: compliance
  image:
    name: fsfe/reuse:latest
    entrypoint: [""]
  script:
    - reuse lint

.go:
  variables:
    CI_GO_VERSION: "latest"

.go-build:
  variables:
    # Disable cgo to avoid linking to host libc.
    CGO_ENABLED: "0"

# Disable cross-build, we want explicit builds defined below.
cross-build:
  rules:
    - when: never

build/linux/armel:
  extends: .go-build
  variables:
    GOOS: linux
    GOARCH: arm
    GOARM: 6
  before_script:
    - !reference [.go-build, before_script]
    - echo "LINUX_ARMEL_CI_JOB_ID=$CI_JOB_ID" >artifacts.linux.armel.env
  artifacts:
    paths: [bin]
    reports:
      dotenv: artifacts.linux.armel.env

build/linux/armhf:
  extends: .go-build
  variables:
    GOOS: linux
    GOARCH: arm
    GOARM: 7
  before_script:
    - !reference [.go-build, before_script]
    - echo "LINUX_ARMHF_CI_JOB_ID=$CI_JOB_ID" >artifacts.linux.armhf.env
  artifacts:
    paths: [bin]
    reports:
      dotenv: artifacts.linux.armhf.env

build/linux/aarch64:
  extends: .go-build
  variables:
    GOOS: linux
    GOARCH: arm64
  before_script:
    - !reference [.go-build, before_script]
    - echo "LINUX_AARCH64_CI_JOB_ID=$CI_JOB_ID" >artifacts.linux.aarch64.env
  artifacts:
    paths: [bin]
    reports:
      dotenv: artifacts.linux.aarch64.env

release:
  stage: deploy
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  tags: [linux, docker, x86_64]
  needs:
    - job: build/linux/armel
      artifacts: true
    - job: build/linux/armhf
      artifacts: true
    - job: build/linux/aarch64
      artifacts: true
  rules:
    - if: $CI_COMMIT_TAG
  script:
    - echo "$LINUX_ARMEL_CI_JOB_ID"
    - echo "$LINUX_ARMHF_CI_JOB_ID"
    - echo "$LINUX_AARCH64_CI_JOB_ID"
  release:
    name: 'Pinsight version $CI_COMMIT_TAG'
    description: "$CI_COMMIT_TAG"
    tag_name: "$CI_COMMIT_TAG"
    assets:
      links:
        - name: "pinsight (Linux, armel) - Pi Zero, Pi 1"
          url: 'https://gitlab.com/zygoon/pinsight/-/jobs/${LINUX_ARMEL_CI_JOB_ID}/artifacts/file/bin/linux_arm/pinsight'
        - name: "pinsight (Linux, armhf) - Pi 2, 3, 4, 5, 02W"
          url: 'https://gitlab.com/zygoon/pinsight/-/jobs/${LINUX_ARMHF_CI_JOB_ID}/artifacts/file/bin/linux_arm/pinsight'
        - name: "pinsight (Linux, aarch64) - Pi 3, 4, 5"
          url: 'https://gitlab.com/zygoon/pinsight/-/jobs/${LINUX_AARCH64_CI_JOB_ID}/artifacts/file/bin/linux_arm64/pinsight'
